﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "Command/BuyCommand.h"
#include "Command/ShopStatsSubsystem.h"

#define GET_STATS_SUBSYSTEM GetWorld()->GetFirstPlayerController()->GetLocalPlayer()->GetSubsystem<UShopStatsSubsystem>()

void UBuyCommand::Execute()
{
	GET_STATS_SUBSYSTEM->ReduceGold(Item.Price);
}

void UBuyCommand::Undo()
{
	GET_STATS_SUBSYSTEM->AddGold(Item.Price);
}
