﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "Command/SellCommand.h"
#include "Command/ShopStatsSubsystem.h"

#define GET_STATS_SUBSYSTEM GetWorld()->GetFirstPlayerController()->GetLocalPlayer()->GetSubsystem<UShopStatsSubsystem>()

void USellCommand::Execute()
{
	GET_STATS_SUBSYSTEM->AddGold(Item.Price);
}

void USellCommand::Undo()
{
	GET_STATS_SUBSYSTEM->ReduceGold(Item.Price);
}
