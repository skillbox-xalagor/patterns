﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "Command/ShopStatsSubsystem.h"

void UShopStatsSubsystem::Initialize(FSubsystemCollectionBase& Collection)
{
	Super::Initialize(Collection);
}

void UShopStatsSubsystem::AddGold(const int32 Amount)
{
	Gold += Amount;
	OnGoldChange.Broadcast(Gold);
}

void UShopStatsSubsystem::ReduceGold(const int32 Amount)
{
	Gold -= Amount;
	OnGoldChange.Broadcast(Gold);
}

void UShopStatsSubsystem::AddReputation(const int32 Amount)
{
	Reputation += Amount;
	OnReputationChange.Broadcast(Reputation);
}

void UShopStatsSubsystem::ReduceReputation(const int32 Amount)
{
	Reputation -= Amount;
	OnReputationChange.Broadcast(Reputation);
}


