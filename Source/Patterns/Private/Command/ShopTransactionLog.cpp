﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "Command/ShopTransactionLog.h"

void UShopTransactionLog::Log(UCommand* Command)
{
	CommandLog.Add(Command);
}

UCommand* UShopTransactionLog::Pop()
{
	if (CommandLog.Num() == 0) return nullptr;
	return CommandLog.Pop();
}
