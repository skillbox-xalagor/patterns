// Fill out your copyright notice in the Description page of Project Settings.


#include "Builder/MapGenerator.h"

void AMapGenerator::GenerateMap()
{
	//Creating necromantic city
	if (!NecroCityGeneratorClassToCreate->IsValidLowLevelFast()) {return;}
	auto NecroGenerator = NewObject<UNecroCityGenerator>(this,NecroCityGeneratorClassToCreate->GetFName(),RF_NoFlags,NecroCityGeneratorClassToCreate.GetDefaultObject());
	auto IGenerator = CastToCanGenerateCity(NecroGenerator);
	UGenerationDirector::GenerateNecroCity(IGenerator);
	Cities.Emplace(dynamic_cast<AActor*>(NecroGenerator->GetResult()));

	// //Creating necromantic city quests
	// auto QuestGenerator = NewObject<UCityQuestGenerator>(this);
	// IGenerator = CastToCanGenerateCity(QuestGenerator);
	// UGenerationDirector::GenerateNecroCity(IGenerator);
	// Quests.Emplace(QuestGenerator->GetResult());
	//
	// //Creating elven city quests
	// auto ElvenGenerator = NewObject<UElvenCityGenerator>(this);
	// IGenerator = CastToCanGenerateCity(ElvenGenerator);
	// UGenerationDirector::GenerateElvenCity(IGenerator);
	// Cities.Emplace(dynamic_cast<AActor*>(ElvenGenerator->GetResult()));
	//
	// //Creating elven city quests
	// IGenerator = CastToCanGenerateCity(QuestGenerator);
	// UGenerationDirector::GenerateElvenCity(IGenerator);
	// Quests.Emplace(QuestGenerator->GetResult());
}
