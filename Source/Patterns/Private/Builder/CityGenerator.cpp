// Fill out your copyright notice in the Description page of Project Settings.


#include "Builder/CityGenerator.h"

UObject* UElvenCityGenerator::GetResult() const
{
	return CityRoot;
}

void UElvenCityGenerator::GenerateGrounds()
{
	SpawnRoot();
	SpawnGrass();
	SpawnTrees();
	SpawnRivers();
}

void UElvenCityGenerator::GenerateCastle()
{
	SpawnTreeCastle();
}

void UElvenCityGenerator::GenerateHouses()
{
	SpawnTreeHouses();
}

void UElvenCityGenerator::SpawnRoot()
{
}

void UElvenCityGenerator::SpawnGrass()
{
}

void UElvenCityGenerator::SpawnTrees()
{
}

void UElvenCityGenerator::SpawnRivers()
{
}

void UElvenCityGenerator::SpawnTreeCastle()
{
}

void UElvenCityGenerator::SpawnTreeHouses()
{
}

UObject* UNecroCityGenerator::GetResult() const
{
	return CityRoot;
}

void UNecroCityGenerator::GenerateGrounds()
{
	FTimerHandle RootTimerHandle;
	GetWorld()->GetTimerManager().SetTimer(RootTimerHandle, this, &UNecroCityGenerator::SpawnRoot, 0.1, false);
	FTimerHandle DeadGroundTimerHandle;
	GetWorld()->GetTimerManager().SetTimer(DeadGroundTimerHandle, this, &UNecroCityGenerator::SpawnDeadGround, 0.2, false);
	FTimerHandle StyxTimerHandle;
	GetWorld()->GetTimerManager().SetTimer(StyxTimerHandle, this, &UNecroCityGenerator::SpawnStyx, 0.3, false);
	FTimerHandle LetaTimerHandle;
	GetWorld()->GetTimerManager().SetTimer(LetaTimerHandle, this, &UNecroCityGenerator::SpawnLeta, 0.4, false);
}

void UNecroCityGenerator::GenerateHouses()
{
	FTimerHandle CemeteryTimerHandle;
	GetWorld()->GetTimerManager().SetTimer(CemeteryTimerHandle, this, &UNecroCityGenerator::SpawnCemetery, 0.5, false);
	FTimerHandle DomTimerHandle;
	GetWorld()->GetTimerManager().SetTimer(DomTimerHandle, this, &UNecroCityGenerator::SpawnDom, 0.6, false);
	FTimerHandle TombsTimerHandle;
	GetWorld()->GetTimerManager().SetTimer(TombsTimerHandle, this, &UNecroCityGenerator::SpawnTombs, 0.7, false);
}

void UNecroCityGenerator::SpawnLeta_Implementation()
{
	if(GEngine)
		GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, TEXT("Spawning Leta"));
}

void UNecroCityGenerator::SpawnStyx_Implementation()
{
	if(GEngine)
		GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, TEXT("Spawning Styx"));
}

void UNecroCityGenerator::SpawnDeadGround_Implementation()
{
	if(GEngine)
		GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, TEXT("Spawning Ground"));
}

void UNecroCityGenerator::SpawnCemetery_Implementation()
{
	if(GEngine)
		GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, TEXT("Spawning Cemetery"));
}

void UNecroCityGenerator::SpawnRoot_Implementation()
{
	if(GEngine)
		GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, TEXT("Spawning Root"));
}

void UNecroCityGenerator::SpawnDom_Implementation()
{
	if(GEngine)
		GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, TEXT("Spawning Dom"));
}

void UNecroCityGenerator::SpawnTombs_Implementation()
{
	if(GEngine)
		GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, TEXT("Spawning Tombs"));
}

void UCityQuestGenerator::GenerateGrounds()
{
	ICanGenerateCity::GenerateGrounds();
}

void UCityQuestGenerator::GenerateCastle()
{
	ICanGenerateCity::GenerateCastle();
}

void UCityQuestGenerator::GenerateHouses()
{
	ICanGenerateCity::GenerateHouses();
}
