// Fill out your copyright notice in the Description page of Project Settings.


#include "Builder/GenerationDirector.h"

void UGenerationDirector::GenerateElvenCity(ICanGenerateCity* Generator)
{
	if (Generator)
	{
		Generator->GenerateGrounds();
		Generator->GenerateCastle();
		Generator->GenerateHouses();
	}
}

void UGenerationDirector::GenerateNecroCity(ICanGenerateCity* Generator)
{
	if (Generator)
	{
		Generator->GenerateGrounds();
		Generator->GenerateHouses();
	}
}
