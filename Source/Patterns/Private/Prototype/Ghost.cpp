// Fill out your copyright notice in the Description page of Project Settings.


#include "Prototype/Ghost.h"

AMonster* AGhost::Clone(FVector Loc)
{
	FActorSpawnParameters Parameters;
	Parameters.Template = this;
	FVector Scale = GetActorScale();
	AMonster* Monster = GetWorld()->SpawnActor<AGhost>(GetClass(), Parameters);
	Monster->SetActorLocation(Loc);
	Monster->SetActorRotation(GetActorRotation());
	Monster->SetActorScale3D(Scale);
	Monster->SetOwner(this);
	return Monster;
}
