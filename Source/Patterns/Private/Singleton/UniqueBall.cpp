// Fill out your copyright notice in the Description page of Project Settings.


#include "Singleton/UniqueBall.h"

// Sets default values
AUniqueBall::AUniqueBall()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

void AUniqueBall::SpawnInstance(FVector Loc)
{
	FRotator Rot = GetActorRotation();
	FVector Scale = GetActorScale();
	Instance = GetWorld()->SpawnActor(InstanceClass, &Loc, &Rot);
}

void AUniqueBall::SetupColor_Implementation()
{
	if (!Instance)
	{
		SpawnInstance(GetActorLocation());
	}
}
