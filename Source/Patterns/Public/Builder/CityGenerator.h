// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "UObject/Object.h"
#include "CanGenerateCity.h"
#include "CityGenerator.generated.h"

/**
 * Class to generate an elven city
 */
UCLASS(Blueprintable, BlueprintType)
class PATTERNS_API UElvenCityGenerator : public UObject, public ICanGenerateCity
{
	GENERATED_BODY()

public:
	virtual UObject* GetResult() const override;
	virtual void GenerateGrounds() override;
	virtual void GenerateCastle() override;
	virtual void GenerateHouses() override;

protected:
	void SpawnRoot();
	void SpawnGrass();
	void SpawnTrees();
	void SpawnRivers();
	void SpawnTreeCastle();
	void SpawnTreeHouses();

private:
	//The root of the city
	UPROPERTY(meta = (AllowPrivateAccess = "true"), BlueprintReadOnly, VisibleAnywhere, Category = City)
	AActor* CityRoot{nullptr};
};

/**
 * Class to generate an necromantic city
 */
UCLASS(Blueprintable, BlueprintType)
class PATTERNS_API UNecroCityGenerator : public UObject, public ICanGenerateCity
{
	GENERATED_BODY()

public:
	virtual UObject* GetResult() const override;
	virtual void GenerateGrounds() override;
	virtual void GenerateHouses() override;

	UFUNCTION(BlueprintNativeEvent)
	void SpawnRoot();
	UFUNCTION(BlueprintNativeEvent)
	void SpawnDeadGround();
	UFUNCTION(BlueprintNativeEvent)
	void SpawnStyx();
	UFUNCTION(BlueprintNativeEvent)
	void SpawnLeta();
	UFUNCTION(BlueprintNativeEvent)
	void SpawnCemetery();
	UFUNCTION(BlueprintNativeEvent)
	void SpawnDom();
	UFUNCTION(BlueprintNativeEvent)
	void SpawnTombs();

private:
	//The root of the city
	UPROPERTY(meta = (AllowPrivateAccess = "true"), BlueprintReadOnly, VisibleAnywhere, Category = City)
	AActor* CityRoot{nullptr};

	//Handle to manage the timer
	FTimerHandle SpawnTimerHandle;
};

/**
 * Class to generate some basic quest structure for given area
 */
UCLASS(Blueprintable, BlueprintType)
class UCityQuestGenerator : public UObject, public ICanGenerateCity
{
	GENERATED_BODY()
public:
	virtual UObject* GetResult() const override
	{
		return QuestsRoot;
	}

	virtual void GenerateGrounds() override;
	virtual void GenerateCastle() override;
	virtual void GenerateHouses() override;

private:
	UObject* QuestsRoot;
};
