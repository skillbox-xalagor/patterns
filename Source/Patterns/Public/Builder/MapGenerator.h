// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "GenerationDirector.h"
#include "CityGenerator.h"
#include "MapGenerator.generated.h"

//Some actor that is the centre point for all cities to be built around
UCLASS(Blueprintable)
class PATTERNS_API AMapGenerator : public AActor
{
	GENERATED_BODY()
	
public:
	//Creates two cities and quests for them
	UFUNCTION(BlueprintCallable)
	void GenerateMap();

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Generators")
	TSubclassOf<UNecroCityGenerator> NecroCityGeneratorClassToCreate = UNecroCityGenerator::StaticClass();


protected:
	UPROPERTY(meta = (AllowPrivateAccess = "true"), BlueprintReadOnly, VisibleAnywhere, Category = Cities)
	TArray<AActor*> Cities;

	UPROPERTY(meta = (AllowPrivateAccess = "true"), BlueprintReadOnly, VisibleAnywhere, Category = Quests)
	TArray<UObject*> Quests;

};

template<class TClass>
ICanGenerateCity* CastToCanGenerateCity(TClass* Object)
{
	return static_cast<ICanGenerateCity*>(Object->GetInterfaceAddress(UCanGenerateCity::StaticClass()));
}