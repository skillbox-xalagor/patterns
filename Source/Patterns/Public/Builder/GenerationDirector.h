// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CanGenerateCity.h"
#include "GenerationDirector.generated.h"

/**
 * A director that knows how to create different type of cities
 */
UCLASS()
class PATTERNS_API UGenerationDirector : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
	public:
	//Step by step generates elven city
	static void GenerateElvenCity(ICanGenerateCity* Generator);
	//Step by step generates nectomantic city
	static void GenerateNecroCity(ICanGenerateCity* Generator);
};
