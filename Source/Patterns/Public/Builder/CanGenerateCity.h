// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "UObject/Interface.h"
#include "CanGenerateCity.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI, meta = (CannotImplementInterfaceInBlueprint))
class UCanGenerateCity : public UInterface
{
	GENERATED_BODY()
};

/**
 * A common interface for all the city generators
 */
class PATTERNS_API ICanGenerateCity
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:
	//Returns an object generated step by step
	UFUNCTION(BlueprintCallable, Category = "City|Generator")
	virtual UObject* GetResult() const PURE_VIRTUAL(ICanGenerateCity::GetResult, return nullptr;);
	//Generate landscape objects
	UFUNCTION(BlueprintCallable, Category = "City|Generator")
	virtual void GenerateGrounds() PURE_VIRTUAL(ICanGenerateCity::GenerateGrounds, );
	//Generate castle-related objects
	UFUNCTION(BlueprintCallable, Category = "City|Generator")
	virtual void GenerateCastle() PURE_VIRTUAL(ICanGenerateCity::GenerateCastle, );
	//Generate houses for citizens
	UFUNCTION(BlueprintCallable, Category = "City|Generator")
	virtual void GenerateHouses() PURE_VIRTUAL(ICanGenerateCity::GenerateHouses, );
};
