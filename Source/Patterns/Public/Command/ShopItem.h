﻿#pragma once

#include "CoreMinimal.h"
#include "ShopItem.generated.h"

USTRUCT(BlueprintType)
struct FShopItem
{
GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FText Name;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 Price;
};
