﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Command.h"
#include "ShopItem.h"
#include "SellCommand.generated.h"

/**
 *
 */
UCLASS(Blueprintable)
class PATTERNS_API USellCommand : public UCommand
{
	GENERATED_BODY()

protected:
	UPROPERTY(BlueprintReadWrite, EditInstanceOnly, meta=(ExposeOnSpawn))
	FShopItem Item;

public:
	virtual void Execute() override;

	virtual void Undo() override;
};
