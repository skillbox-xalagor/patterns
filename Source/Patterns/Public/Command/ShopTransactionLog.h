﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "ShopTransactionLog.generated.h"

class UCommand;

/**
 *
 */
UCLASS(Blueprintable)
class PATTERNS_API UShopTransactionLog : public UActorComponent
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintCallable)
	void Log(UCommand* Command);

	UFUNCTION(BlueprintCallable)
	UCommand* Pop();

private:
	TArray<UCommand*> CommandLog;
};
