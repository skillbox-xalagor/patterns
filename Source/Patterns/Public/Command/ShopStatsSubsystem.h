﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Subsystems/LocalPlayerSubsystem.h"
#include "ShopStatsSubsystem.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnGoldChange, int32, NewValue);

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnReputationChange, int32, NewValue);

/**
 * Singleton for accessing Shop Stats manipulation
 */
UCLASS()
class PATTERNS_API UShopStatsSubsystem : public ULocalPlayerSubsystem
{
	GENERATED_BODY()

public:
	virtual void Initialize(FSubsystemCollectionBase& Collection) override;

private:
	int32 Gold;
	int32 Reputation;

public:
	UFUNCTION(BlueprintCallable, BlueprintPure)
	FORCEINLINE int32 GetGold()
	{
		return Gold;
	}

	UFUNCTION(BlueprintCallable)
	void AddGold(int32 Amount);

	UFUNCTION(BlueprintCallable)
	void ReduceGold(int32 Amount);

	UPROPERTY(BlueprintAssignable)
	FOnGoldChange OnGoldChange;

	UFUNCTION(BlueprintCallable, BlueprintPure)
	FORCEINLINE int32 GetReputation()
	{
		return Reputation;
	}

	UFUNCTION(BlueprintCallable)
	void AddReputation(int32 Amount);

	UFUNCTION(BlueprintCallable)
	void ReduceReputation(int32 Amount);

	UPROPERTY(BlueprintAssignable)
	FOnReputationChange OnReputationChange;
};
