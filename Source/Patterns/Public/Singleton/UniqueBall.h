// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "UniqueBall.generated.h"

UCLASS()
class PATTERNS_API AUniqueBall : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AUniqueBall();

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
	void SetupColor();

	UPROPERTY(BlueprintReadWrite)
	AActor* Instance;

	UPROPERTY(EditAnywhere)
	TSubclassOf<AActor> InstanceClass;

private:
	void SpawnInstance(FVector Loc);

};
