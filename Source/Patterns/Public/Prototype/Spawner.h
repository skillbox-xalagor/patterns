// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Monster.h"
#include "GameFramework/Actor.h"
#include "Spawner.generated.h"

UCLASS()
class PATTERNS_API ASpawner : public AActor
{
	GENERATED_BODY()
public:
	UFUNCTION(BlueprintCallable)
	AMonster* SpawnMonster();

private:
	UPROPERTY(EditInstanceOnly, Category = Prototype)
	AMonster* Prototype;

};
