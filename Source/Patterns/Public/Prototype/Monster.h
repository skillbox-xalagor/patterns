// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Monster.generated.h"

UCLASS()
class PATTERNS_API AMonster : public AActor
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintCallable)
	virtual AMonster* Clone(FVector Loc) PURE_VIRTUAL(AMonster::Clone, return nullptr;);

};
