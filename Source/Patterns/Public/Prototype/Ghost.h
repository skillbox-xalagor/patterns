// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Prototype/Monster.h"
#include "Ghost.generated.h"

/**
 * 
 */
UCLASS()
class PATTERNS_API AGhost : public AMonster
{
	GENERATED_BODY()

public:
	virtual AMonster* Clone(FVector Loc) override;

private:
	int Health = 0;
	int Speed = 0;


};
